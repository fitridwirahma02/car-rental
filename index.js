const express = require("express")
const app = express()
const { PORT = 5000 } = process.env
const bodyParser = require("body-parser")

const swaggerUI = require("swagger-ui-express")
const swaggerJSON = require("./swagger.json")

app.use(express.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.get("/api-docs", (req,res) => {
    res.json(swaggerJSON)
  })
  
app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON))  

// import controllers
const authController = require("./controllers/authController");
const carsController = require("./controllers/carsController");

// improt middlewares
const middlewares = require("./middlewares/auth");

// define routes auth
// register member
app.post("/auth/register", authController.register);

// register admin
app.post("/auth/register/admin", middlewares.authenticate, middlewares.isSuperAdmin, authController.register);

app.post("/auth/login", authController.login);
app.get("/auth/profile", middlewares.authenticate, authController.currentUser);

// define routes CRUD
app.get("/cars", middlewares.authenticate, middlewares.roles, carsController.getCars);
app.post("/cars/create", middlewares.authenticate, middlewares.roles, carsController.create);
app.put("/cars/update/:id", middlewares.authenticate, middlewares.roles, carsController.update);
app.delete("/cars/delete/:id", middlewares.authenticate, middlewares.roles, carsController.deleted);

app.listen(PORT, () => {
    console.log(`express running on port ${PORT}`)
})