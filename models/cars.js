'use strict';

const {
  v4: uuidv4
} = require('uuid');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  cars.init({
    name: DataTypes.STRING,
    photo: DataTypes.TEXT,
    price: DataTypes.INTEGER,
    size_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    is_deleted: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'cars',
  });
  cars.beforeCreate(async (data, options) =>
    data.id = uuidv4())
  return cars;
};