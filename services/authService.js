const usersRepository = require("../repositories/usersRepository");
const bycrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {
    JWT
} = require("../lib/const");
const SALT_ROUND = 10;

class authService {
    static async register({
        username,
        password,
        role,
        address,
        phone_number,
        fullname
    }) {
        try {
            if (!username) {
                return {
                    status: false,
                    code_status: 400,
                    message: "username wajib diisi",
                    data: {
                        registered_Users: null,
                    }
                }
            };

            if (!address) {
                return {
                    status: false,
                    code_status: 400,
                    message: "address wajib diisi",
                    data: {
                        registered_Users: null,
                    },
                };
            }

            if (!phone_number) {
                return {
                    status: false,
                    code_status: 400,
                    message: "phone number wajib diisi",
                    data: {
                        registered_Users: null,
                    },
                };
            }

            if (!fullname) {
                return {
                    status: false,
                    code_status: 400,
                    message: "fullname wajib diisi",
                    data: {
                        registered_Users: null,
                    },
                };
            }

            if (!role) {
                return {
                    status: false,
                    code_status: 400,
                    message: "role wajib diisi",
                    data: {
                        registered_Users: null,
                    },
                };
            }

            if (!password) {
                return {
                    status: false,
                    code_status: 400,
                    message: "password wajib diisi",
                    data: {
                        registered_Users: null,
                    },
                };
            } else if (password.length < 8) {
                return {
                    status: false,
                    code_status: 400,
                    message: "password minimal 8 karakter",
                    data: {
                        registered_Users: null,
                    },
                };
            }

            const getByUsername = await usersRepository.getByUsername({
                username
            });

            if (getByUsername) {
                return {
                    status: false,
                    code_status: 400,
                    message: "username sudah terdaftar",
                    data: {
                        registered_Users: null,
                    },
                };
            } else {
                const hashingPassword = await bycrypt.hash(password, SALT_ROUND);
                const regsiteredUsers = await usersRepository.register({
                    username,
                    password: hashingPassword,
                    role,
                    address,
                    phone_number,
                    fullname
                });

                return {
                    status: true,
                    code_status: 201,
                    message: "Users berhasil registrasi",
                    data: {
                        registered_Users: regsiteredUsers,
                    },
                };
            }
        } catch (err){
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    registered_Users: null,
                },
            };
        }
    }

    static async login({
        username,
        password
    }){
        try{
            if (!username) {
                return {
                    status: false,
                    code_status: 400,
                    message: "username wajib diisi",
                    data: {
                        login_Users: null,
                    }
                }
            };
    
            if (!password) {
                return {
                    status: false,
                    code_status: 400,
                    message: "password wajib diisi",
                    data: {
                        login_Users: null,
                    },
                };
            } else if (password.length < 8) {
                return {
                    status: false,
                    code_status: 400,
                    message: "password minimal 8 karakter",
                    data: {
                        login_Users: null,
                    },
                };
            }
    
            const getUsers = await usersRepository.getByUsername({
                username
            });
    
            if(!getUsers){
                return{
                    status: false,
                    code_status: 400,
                    message: "username belum terdaftar!",
                    data:{
                        login_Users: null,
                    },
                };
            } else{
                const passwordMatching = await bycrypt.compare(password, getUsers.password);
    
                if (passwordMatching){
                    const token = jwt.sign({
                        id: getUsers.id,
                        username: getUsers.username,
                    }, JWT.SECRET, {
                        expiresIn: JWT.EXPIRED,
                    });
    
                    return {
                        status: true,
                        code_status: 200,
                        message: "anda berhasil login",
                        data: {
                            token,
                        },
                    };
                } else {
                    return {
                        status: false,
                        code_status: 400,
                        message: "password anda salah, mohon isi ulang",
                        data: {
                            login_Users: null,
                        },
                    };
                }
            }
        }catch (err){
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    login_Users: null,
                },
            };
        }
    }
}

module.exports = authService;