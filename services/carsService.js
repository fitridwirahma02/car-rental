const carsRepository = require("../repositories/carsRepository");

class carsService {
    static async create({
        createdBy,
        updatedBy,
        name,
        photo,
        price,
        size_id
    }) {
        try {
            if (!name) {
                return {
                    status: false,
                    code_status: 400,
                    message: "name wajib diisi",
                    data: {
                        created_Cars: null,
                    }
                }
            };

            if (!photo) {
                return {
                    status: false,
                    code_status: 400,
                    message: "photo wajib diisi",
                    data: {
                        created_Cars: null,
                    }
                }
            };

            if (!price) {
                return {
                    status: false,
                    code_status: 400,
                    message: "price wajib diisi",
                    data: {
                        created_Cars: null,
                    }
                }
            };

            if (!size_id) {
                return {
                    status: false,
                    code_status: 400,
                    message: "size wajib diisi",
                    data: {
                        created_Cars: null,
                    }
                }
            };

            const createdCars = await carsRepository.create({
                createdBy,
                updatedBy,
                name,
                photo,
                price,
                size_id,
            });

            return {
                status: true,
                code_status: 201,
                message: "cars berhasil dibuat",
                data: {
                    createdCars: createdCars,
                },
            };
        } catch (err) {
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    created_Cars: null,
                },
            };
        }
    }

    static async getCars() {
        try {
            const getCars = await carsRepository.getCars();

            return {
                status: true,
                code_status: 200,
                message: "data cars berhasil ditampilkan",
                data: {
                    getCars: getCars,
                },
            };
        } catch (err) {
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    getCars: null,
                },
            };
        }
    }

    static async update({
        id,
        updatedBy,
        name,
        photo,
        price,
        size_id
    }) {
        try {
            if (!name) {
                return {
                    status: false,
                    code_status: 400,
                    message: "name wajib diisi",
                    data: {
                        created_Cars: null,
                    }
                }
            };

            if (!photo) {
                return {
                    status: false,
                    code_status: 400,
                    message: "photo wajib diisi",
                    data: {
                        created_Cars: null,
                    }
                }
            };

            if (!price) {
                return {
                    status: false,
                    code_status: 400,
                    message: "price wajib diisi",
                    data: {
                        created_Cars: null,
                    }
                }
            };

            if (!size_id) {
                return {
                    status: false,
                    code_status: 400,
                    message: "size wajib diisi",
                    data: {
                        created_Cars: null,
                    }
                }
            };

            const updatedCars = await carsRepository.update({
                id,
                updatedBy,
                name,
                photo,
                price,
                size_id,
            });

            return {
                status: true,
                code_status: 201,
                message: "data cars berhasil dupdate",
                data: {
                    updatedCars: updatedCars,
                },
            };
        } catch (err) {
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    updated_Cars: null,
                },
            };
        }
    }

    static async deleted({
        id,
        is_deleted
    }) {
        try {
            const deletedCars = await carsRepository.deleted({
                id,
                is_deleted
            });

            return {
                status: true,
                code_status: 200,
                message: "data cars berhasil dihapus",
                data: {
                    deletedCars: deletedCars,
                },
            }
        } catch (err) {
            return {
                status: false,
                code_status: 500,
                message: err.message,
                data: {
                    deleted_Cars: null,
                },
            };
        }

    }

}

module.exports = carsService;