const carsService = require("../services/carsService");

const create = async (req, res) => {
    const {
        name,
        photo,
        price,
        size_id,
    } = req.body;

    const createdBy = req.user.fullname
    const updatedBy = req.user.fullname

    const {
        status,
        code_status,
        message,
        data
    } = await carsService.create({
        createdBy,
        updatedBy,
        name,
        photo,
        price,
        size_id,
    });

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
};

const getCars = async (req, res) => {
    const {
        status,
        code_status,
        message,
        data
    } = await carsService.getCars();

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
}


const update = async (req, res) => {
    const {
        id
    } = req.params;

    const {
        name,
        photo,
        price,
        size_id,
    } = req.body;

    const updatedBy = req.user.fullname

    const {
        status,
        code_status,
        message,
        data
    } = await carsService.update({
        id,
        updatedBy,
        name,
        photo,
        price,
        size_id,
    });

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
};

const deleted = async (req, res) => {
    const {
        id
    } = req.params;

    const is_deleted = req.user.fullname

    const {
        status,
        code_status,
        message,
        data
    } = await carsService.deleted({
        id,
        is_deleted
    });

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
};

module.exports = {
    create,
    getCars,
    update,
    deleted
}