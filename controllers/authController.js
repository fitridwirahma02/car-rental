const authService = require("../services/authService");

const register = async (req, res) => {
    const {
        username,
        password,
        role,
        address,
        phone_number,
        fullname
    } = req.body;

    const {
        status,
        code_status,
        message,
        data
    } = await authService.register({
        username,
        password,
        role,
        address,
        phone_number,
        fullname
    });

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
};

const login = async (req, res) => {
    const {
        username,
        password,
    } = req.body;

    const {
        status,
        code_status,
        message,
        data
    } = await authService.login({
        username,
        password,
    });

    res.status(code_status).send({
        status: status,
        message: message,
        data: data,
    });
}

const currentUser = async (req, res) => {
    const currentUser = req.user;

    res.status(200).send({
        status: true,
        message: "Get current user success.",
        data: {
            user: currentUser,
        },
    });
}

module.exports = {
    register,
    login,
    currentUser
}