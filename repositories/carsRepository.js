const {
    cars
} = require("../models");

class carsRepository {
    static async create({
        createdBy,
        updatedBy,
        name,
        photo,
        price,
        size_id
    }) {
        const created_Cars = await cars.create({
            createdBy,
            updatedBy,
            name,
            photo,
            price,
            size_id
        })

        return created_Cars;
    }

    static async getCars() {
        const getCars = await cars.findAll();

        return getCars;
    }

    static async update({
        id,
        updatedBy,
        name,
        photo,
        price,
        size_id
    }) {
        const updated_cars = await cars.update({
            updatedBy,
            name,
            photo,
            price,
            size_id
        }, {
            where: {
                id
            }
        });

        return updated_cars;
    }

    static async deleted({
        id
    }) {
        const deletedCars = await cars.destroy({
            where: {
                id
            }
        });

        return deletedCars;
    }
}

module.exports = carsRepository;