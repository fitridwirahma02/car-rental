const {
    users
} = require("../models");

class usersRepository {
    static async getByUsername({
        username
    }) {
        const getUsersByUsername = await users.findOne({
            where: {
                username
            }
        })
        return getUsersByUsername;
    }

    static async register({
        username,
        password,
        role,
        address,
        phone_number,
        fullname
    }) {
        const registered_Users = users.create({
            username,
            password,
            role,
            address,
            phone_number,
            fullname
        })

        return registered_Users;
    }
}

module.exports = usersRepository;